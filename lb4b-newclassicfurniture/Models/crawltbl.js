var mongoose = require("mongoose");

var Schema = mongoose.Schema;
 
var crawlschema = new Schema({ 
     category: { type: String},
     Subcategory : [{
     	_id: false,
        name : { type: String},
        product: [{
        	_id: false,
        	url: { type: String}
                }]
        	
      }],
      versionKey: false 
});
 
module.exports.crawlschema = crawlschema; //Export bugSchema so that models.js can access it.