const mongoose = require("mongoose");
const MongoClient = require('mongodb').MongoClient;
const ObjectID = require('mongodb').ObjectID;

var mdbtbl = require("./crawltbl"); /* Imports the Bugs module. It contains the bug schema we need. */

function dbConnect(){

	mongoose.connect("mongodb://Admin:User123@ds251877.mlab.com:51877/selva_webscraping",{ useNewUrlParser: true }); //Test is the database name. 

	let db = mongoose.connection;

	//const client = await MongoClient.connect("mongodb://jwt:abc123@ds121382.mlab.com:21382/lb4bjob",{ useNewUrlParser: true });
	 
	db.on("error", console.error.bind(console, "Connection error:"));


	return new Promise((resolve, reject) =>{
		db.once("open", function(callback) {
		   resolve(true); 
		})		
	});
 };

 async function save(data,collectionname){
 	    const  DocCount = await mongoose.connection.db.collection(collectionname).countDocuments({});
        
 	    if (DocCount > 0){
 	       mongoose.connection.db.collection(collectionname).drop();
 	    }

 	    await mongoose.connection.db.createCollection(collectionname,{});
 	    return await mongoose.connection.db.collection(collectionname).insertMany(data);
 		//const model =  mongoose.model(collectionname, mdbtbl.crawlschema,collectionname); 
 		//const collection = model(data);
 		//return await collection.save();


 };

 function dbClose(){
 	return mongoose.connection.close();
 };

 function db_Close(){
 	return mongoose.connection.close();
 }; 

async function GetCollection(collectionname){

 	let Sucess = await dbConnect(); 

 	if(Sucess){

 	    const  DocCount = await mongoose.connection.db.collection(collectionname).countDocuments({});
        
 	    if (DocCount > 0){
 	       const DocumentArr=  await mongoose.connection.db.collection(collectionname).find().toArray();
           dbClose();
           return DocumentArr;       	                          
 	    }else{
 	       dbClose();
          return [];
 	    }

 	}
 };

async function UpdateCollection(collectionname,id,SubCatName,product){
	/*let Sucess = await dbConnect(); 

	if(Sucess){
	  try{	
	    const writeresult = await mongoose.connection.db.collection(collectionname).updateOne(
		   	{"_id":ObjectID(id)},
		   	{$set: { "Subcategory.$[elem].product" : product } },
		    {
		     multi: false,
		     arrayFilters: [ { "elem.name": SubCatName } ]
		    });	

	    //dbClose();
	    console.log(`Subcategory : ${SubCatName} : Product length : ${product.length}`)
	    return true;
	    }catch(e){
	    	//dbClose();
	    	return false;
	    }		
	}*/

	  try{	
	    const writeresult = await mongoose.connection.db.collection(collectionname).updateOne(
		   	{"_id":ObjectID(id)},
		   	{$set: { "Subcategory.$[elem].product" : product } },
		    {
		     multi: false,
		     arrayFilters: [ { "elem.name": SubCatName } ]
		    });	

	    //dbClose();
	    console.log(`Subcategory : ${SubCatName} : Product length : ${product.length}`)
	    return true;
	    }catch(e){
	    	console.log(`Error From MongoDB : ${e}`);
	    	return false;
	    }			
};


	


 function dbClose(){
 	return mongoose.connection.close();
 };


 
module.exports.dbConnect = dbConnect; /* Export the Bug model so index.js can access it. */
module.exports.save = save; /* Export the Bug model so index.js can access it. */
module.exports.dbClose = dbClose; /* Export the Bug model so index.js can access it. */
module.exports.GetCollection = GetCollection; /* Export the Bug model so index.js can access it. */
module.exports.UpdateCollection = UpdateCollection; /* Export the Bug model so index.js can access it. */