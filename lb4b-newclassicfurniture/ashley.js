var cheerio = require('cheerio');
var axios = require('axios');
var https = require('https');
var async = require('async');
const Models = require("./Models/models"); 

let scrapqueue;


async function dorequest(Product){

	//const url = "https://www.ashleydirect.com/Catalog/ItemDetail/Item/3620621?env=AFI&whses=5&searchscroll=0";
   process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';


return new Promise((resolve, reject) => {
      axios.get(Product.url,{
            headers: {
                Cookie: "_ga=GA1.2.466929446.1541571345; sitelogin=si=eJzzTwzxKQgwSjIwMPfyKA3283YyTjIJy002iPDI8THJTMwpstTOSHGLcsnxN640DQsuCo1Mq/DKL3EtSPcKcc/2D9F2DwjyjSgycvK0BQClhxh8; _gid=GA1.2.1109947061.1544945806; GUID=24F8E147-76DD-4174-B832-1FE7DA35C5F5; _gat=1",
                Accept: "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8"
            }      	
      })
      .then(async (response) => {
        if(response.status === 200) {
          ProductObj	= await getselector(response.data,Product);
          //ProductObj  = await getselectorTest(response.data,Product);
          resolve(ProductObj);

        }else {
          reject("Error");
        }
      })
     .catch((error)=>reject("Error"));
  });
};

async function getselectorTest(html,product){

let $ = cheerio.load(html);
//console.log(product.url);
return new Promise(async (resolve, reject) => {
  try{  

    let Labels;
    let element;

    element = $('img.img-responsive.ImageTooltip')[0];
    product.image = element.attribs.src;

    let price = $('#spanPrice')[0].children[0].data;
    let AdjPrice = 0;
    let cents = 0;


    if (price.indexOf('.') > 0){
      AdjPrice = price.slice(1,price.indexOf('.'));
      cents = price.slice(price.indexOf('.')+1);
    }else{
      AdjPrice = price.slice(1,);
    }

    if (AdjPrice > 0){
      AdjPrice *= 2;
    }

    if (cents > 50 && AdjPrice > 0){
      AdjPrice += 1;
    }

    product.price = AdjPrice;


    Labels = $('div.col-xl-3.col-lg-4.col-md-3.col-sm-5');
    element = $('div.col-xl-9.col-lg-8.col-md-9.col-sm-7'); 

    product.id = "";
    product.name = "";
    product.itemseries = "";
    product.color = "";
    product.style = "";
    product.dimensions = "";
    product.moredimensions = "";
    product.itemdescription = "";
    product.itemfeatures = "";



    for(let i = 0;i<Labels.length;i++){

     const NodeName =  await FindNode(Labels[i]);


      if(NodeName.length > 0) {

        if(NodeName === "Master Item Number:"){
            product.id = await  FindNode(element[i]);  
        }

        if(NodeName === "Description:"){
            product.name = await  FindNode(element[i]);  
        }         


        if(NodeName === "Series:"){
            product.itemseries = await FindNode(element[i]);  
        }

        if(NodeName === "Colors:"){
            product.color = await FindNode(element[i]);  
        } 

        if(NodeName === "Style:"){
            product.style = await FindNode(element[i]);  
        }     
      }   
    }


   const doms = $('div.col-xs-12.col-sm-12.col-md-12.col-lg-9.col-xl-8');
   const dom =doms[0];

   Labels = [];
   element = [];

    if (dom.hasOwnProperty('children') && dom.children.length > 0){

      for (let c=0;c<dom.children.length;c++){
        const child = dom.children[c];
        if(child.hasOwnProperty('attribs') && child.attribs.class === "row"
           && child.hasOwnProperty('children') && child.children.length > 0){

          const labeldiv = $(child).find('div.col-sm-3.col-md-3.col-lg-3.col-xl-2');
          const valuediv = $(child).find('div.col-sm-9.col-md-9.col-lg-9.col-xl-10');

          if (labeldiv !== undefined && labeldiv.length > 0 && valuediv !== undefined && valuediv.length > 0){
            Labels.push(labeldiv[0]);
            element.push(valuediv[0]);
          }
        }

      }

    }

    for(let i = 0;i<Labels.length;i++){
     
     const NodeName =  await FindNode(Labels[i]);


      if(NodeName.length > 0) {

        if(NodeName === "Dimensions (Confirmed):"){
            product.dimensions = await  FindValue(element[i]);  
        }

        if(NodeName === "More Dimensions:"){
            product.moredimensions = await  FindValue(element[i]);  
        }         


        if(NodeName === "Item Description:"){
            product.itemdescription = await FindValue(element[i]);  
        }

        if(NodeName === "Item Features:"){
            product.itemfeatures = await FindValue(element[i]);  
        }     
      }   

    }
    resolve(product);
   }catch(error){
      console.log(error);
     return("Error");
   } 

 });   

};


async function getselector(html,product){

let $ = cheerio.load(html);

return new Promise(async (resolve, reject) => {
  try{  

    let Labels;
    let element;

    element = $('img.img-responsive.ImageTooltip')[0];
    product.image = element.attribs.src;

    let price = $('#spanPrice')[0].children[0].data;
    let AdjPrice = 0;
    let cents = 0;


    if (price.indexOf('.') > 0){
      AdjPrice = price.slice(1,price.indexOf('.'));
      cents = price.slice(price.indexOf('.')+1);
    }else{
      AdjPrice = price.slice(1,);
    }

    if (AdjPrice > 0){
      AdjPrice *= 2;
    }

    if (cents > 50 && AdjPrice > 0){
      AdjPrice += 1;
    }

    product.price = AdjPrice;


    Labels = $('div.col-xl-3.col-lg-4.col-md-3.col-sm-5');
    element = $('div.col-xl-9.col-lg-8.col-md-9.col-sm-7'); 

    product.id = "";
    product.name = "";
    product.itemseries = "";
    product.color = "";
    product.style = "";
    product.dimensions = "";
    product.moredimensions = "";
    product.itemdescription = "";
    product.itemfeatures = "";



    for(let i = 0;i<Labels.length;i++){

     const NodeName =  await FindNode(Labels[i]);


      if(NodeName.length > 0) {

        if(NodeName === "Master Item Number:"){
            product.id = await  FindNode(element[i]);  
        }

        if(NodeName === "Description:"){
            product.name = await  FindNode(element[i]);  
        }         


        if(NodeName === "Series:"){
            product.itemseries = await FindNode(element[i]);  
        }

        if(NodeName === "Colors:"){
            product.color = await FindNode(element[i]);  
        } 

        if(NodeName === "Style:"){
            product.style = await FindNode(element[i]);  
        }     
      }   
    }


   const doms = $('div.col-xs-12.col-sm-12.col-md-12.col-lg-9.col-xl-8');
   const dom =doms[0];

   Labels = [];
   element = [];

    if (dom.hasOwnProperty('children') && dom.children.length > 0){

      for (let c=0;c<dom.children.length;c++){
        const child = dom.children[c];
        if(child.hasOwnProperty('attribs') && child.attribs.class === "row"
           && child.hasOwnProperty('children') && child.children.length > 0){

          const labeldiv = $(child).find('div.col-sm-3.col-md-3.col-lg-3.col-xl-2');
          const valuediv = $(child).find('div.col-sm-9.col-md-9.col-lg-9.col-xl-10');

          if (labeldiv !== undefined && labeldiv.length > 0 && valuediv !== undefined && valuediv.length > 0){
            Labels.push(labeldiv[0]);
            element.push(valuediv[0]);
          }
        }

      }

    }

    for(let i = 0;i<Labels.length;i++){
     
     const NodeName =  await FindNode(Labels[i]);


      if(NodeName.length > 0) {

        if(NodeName === "Dimensions (Confirmed):"){
            product.dimensions = await  FindValue(element[i]);  
        }

        if(NodeName === "More Dimensions:"){
            product.moredimensions = await  FindValue(element[i]);  
        }         


        if(NodeName === "Item Description:"){
            product.itemdescription = await FindValue(element[i]);  
        }

        if(NodeName === "Item Features:"){
            product.itemfeatures = await FindValue(element[i]);  
        }     
      }   

    }
    resolve(product);
   }catch(error){
      console.log(error);
     return("Error");
   } 

 });  

};

async function FindNode(ParentNode){

  let NodeName="";
   
  if (ParentNode.hasOwnProperty('children') && ParentNode.children.length > 0 ){

    for (let i = 0;i<ParentNode.children.length;i++ ){
      if(ParentNode.children[i].hasOwnProperty('type')){

        if(ParentNode.children[i].type === "tag" &&  (ParentNode.children[i].name === "div" || 
                                                      ParentNode.children[i].name === "strong" || 
                                                      ParentNode.children[i].name === "a" ||                                                       
                                                      ParentNode.children[i].name === "span")){
          if(NodeName.length > 0 ){
            NodeName = NodeName  + await FindNode(ParentNode.children[i]);
          }else{
            NodeName = await FindNode(ParentNode.children[i]);
          }
        }

        if(ParentNode.children[i].type === "text"){

          if(NodeName.length > 0 ){
            NodeName = NodeName + ParentNode.children[i].data.trim();  
          }else{
            NodeName = ParentNode.children[i].data.trim();  
          }          

        }        
      }

    }
  }
 return NodeName;
}

async function FindValue(ParentNode){

  let NodeValue="";
   
  if (ParentNode.hasOwnProperty('children') && ParentNode.children.length > 0 ){

    for (let i = 0;i<ParentNode.children.length;i++ ){
      if(ParentNode.children[i].hasOwnProperty('type')){

        if(ParentNode.children[i].type === "tag" &&  ParentNode.children[i].name === "div"){

          const retvalue =  await FindValue(ParentNode.children[i]);

          if(retvalue.length > 0 ){
            if (NodeValue.length > 0){
              NodeValue = NodeValue + '\n' + retvalue;
            }else{
              NodeValue = retvalue;
            }
          }
        }

        if(ParentNode.children[i].type === "text"){

          if(ParentNode.children[i].data.trim().length > 0 ){
            if(NodeValue.length > 0){
              NodeValue = NodeValue + '\n' + ParentNode.children[i].data.trim();  
            }else{
              NodeValue = ParentNode.children[i].data.trim();  
            }
          }          

        }        
      }

    }
  }

 return NodeValue;
}

async function Pushqueue(SubCategory){

    scrapqueue.push(SubCategory, pushcallback)

}

function pushcallback(err){

  if(err){
    console.log("Program Failed");
    console.log(err);

    scrapqueue.kill();
    }
    else{
      //console.log("Queue completed");
    }
}

async function initqueue(){

   scrapqueue = async.queue(function(SubCat,callback){

    (async function() {
      try {
        //const html = await DoCallRequest(jsonobj.url);
        const Products = [];
        //console.log(SubCat.name);

        if (Array.isArray(SubCat.product) && SubCat.product.length > 0){

            const textPromises = SubCat.product.map(async product => {
                const response = await dorequest(product);
                return response;
            });


            let i = 1;

            // log them in sequence
      			for (const textPromise of textPromises) {
      			    //console.log(await textPromise);
                Products.push(await textPromise);
      			    if(i++ === SubCat.product.length){
      			      SubCat.product = Products;
      			      //console.log(JSON.stringify(SubCat.products));	
                  const returnval = await Models.UpdateCollection("Ashleyscrap",SubCat.id,SubCat.name,Products);
                  callback(null);
                  break;
      			    }
      			}           
        }else{
          callback(null);
        }
      } catch (error) {
        callback(error);
       }
      })();

      

   },10);

  scrapqueue.drain = function() {
     Models.dbClose();
     console.log("Process Ended : " + new Date().toLocaleString().replace(/T/, ' ').replace(/\..+/, ''));
  };     

};

async function SaveCrawlCopy(categoryObj,collectionname){

    await Models.save(categoryObj,collectionname);
}


async function AshleyScrap(){


  //const Models = require("./Models/models"); 
  let CategoryArr = [];
  let Categories = [];

  const CopyCrawldata = false;

  if(CopyCrawldata){

    CategoryArr  = await Models.GetCollection("Ashley");

    let Sucess = await Models.dbConnect(); 
    if(Sucess){
      await Models.save(CategoryArr,"Ashleyscrap");
      CategoryArr  = await Models.GetCollection("Ashleyscrap");
      Models.dbClose();
    }
  }else{
    CategoryArr  = await Models.GetCollection("Ashleyscrap");
  }

  
  for (const Category of CategoryArr) {
    if (Category.category === "Bedroom"){
      Categories.push(Category);
    }
  }


  console.log("Process start : " + new Date().toLocaleString().replace(/T/, ' ').replace(/\..+/, ''));
  let Sucess = await Models.dbConnect(); 

  if(Sucess){

    initqueue();

    for (const CategoryObj of Categories) {

      const UniqId = CategoryObj._id;
      let Subcategories = CategoryObj.Subcategory;

      //const FailedSubCats = [];

        if(Array.isArray(Subcategories)){
         

         /* for(let i = 0;i<Subcategories.length;i++) {

            const prod = Subcategories[i].product ;

            for(let j =0;j<prod.length;j++){

              if(typeof prod[j].id === "undefined"){
                console.log(Subcategories[i].name);
                FailedSubCats.push(Subcategories[i]);
                break;
              }

            }
          }*/
  
          //Subcategories = FailedSubCats;

           Subcategories.forEach(SubCategory=>{
              SubCategory.id = UniqId;
                Pushqueue(SubCategory);
           });

           //let SubCategory = Subcategories[1];
           //SubCategory.id = UniqId;
           //Pushqueue(SubCategory);           
        }    

    }
  }

};

async function CountScrapedItem(){
  let CategoryArr = [];
  let TotalProduct = 0;
  let TotalSubcat = 0;
  
  CategoryArr  = await Models.GetCollection("Ashleyscrap");

  for (const Category of CategoryArr) {

    const Subcategories = Category.Subcategory;  
    console.log(`Category => ${Category.category}`);

    if(Array.isArray(Subcategories)){
     
     TotalSubcat = TotalSubcat + Subcategories.length
     console.log(`Total Subcategory ${Subcategories.length} in Category ${Category.category}`);

     for(let i = 0;i<Subcategories.length;i++) {

        TotalProduct += Subcategories[i].product.length ;
         

      }
    }  


  }
  console.log(`Total SubCaategory : ${TotalSubcat}`);
  console.log(`Total Products : ${TotalProduct}`);
}


async function testdata(){
  product = {
    url: "https://www.ashleydirect.com/Catalog/ItemDetail/Item/4110121?env=AFI&whses=5"
  };

  const value = await dorequest(product);
  console.log(value);
}

//testdata();

//AshleyScrap();
CountScrapedItem();

//dorequest();




